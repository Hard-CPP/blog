<?php

namespace AppBundle\Twig;

use Component\Dictionary\AbstractDictionary;

class DictionaryExtension extends \Twig_Extension
{
    private $dictionary;
    private $twig;

    public function __construct(AbstractDictionary $dictionary, \Twig_Environment $twig)
    {
        $this->dictionary = $dictionary;
        $this->twig       = $twig;
    }

    public function getFilters()
    {
        return [
            new \Twig_SimpleFilter("dictionary", [$this, "dictionary"])
        ];
    }

    public function dictionary(string $str)
    {
        $words = str_word_count($str, 1);

        foreach ($words as $word) {
            if (null !== $definition = $this->dictionary->getDefinition($word)) {
                // $str = str_replace($word,sprintf('<a href="#" title="%s" style="color: red">%s</a>', $definition, $word),$str); // Solution bas de gamme

                // Solution avec template twig => mieux !
                $html = $this->twig->render("@App/dictionary/tpl.html.twig", ["word" => $word, "definition" => $definition]); // on génère le code HTML avec twig
                $str = str_replace($word, $html, $str); // on remplace chaque instance du mot sans la châine da caractère fourni pas le code html généré au dessus
            }
        }

        return $str;
    }
}