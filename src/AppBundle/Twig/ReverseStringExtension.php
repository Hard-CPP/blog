<?php

namespace AppBundle\Twig;

class ReverseStringExtension extends \Twig_Extension
{
    /**
     * @return array
     */
    public function getFilters()
    {
        return [
            new \Twig_SimpleFilter("strrev", [$this, "stringReverse"])
        ];
    }

    /**
     * @param string $str
     *
     * @return string
     */
    public function stringReverse(string $str) : string
    {
        return strrev($str);
    }
}