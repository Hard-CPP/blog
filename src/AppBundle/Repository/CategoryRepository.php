<?php

namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;

class CategoryRepository extends EntityRepository
{
    public function findCategory($category_id)
    {
        return $this->createQueryBuilder("c")
            ->select("c", "p")
            ->leftJoin("c.posts", "p")
            ->where("c.id = :cid")
            ->setParameter("cid", $category_id)
            ->getQuery()
            ->getOneOrNullResult();
    }
}