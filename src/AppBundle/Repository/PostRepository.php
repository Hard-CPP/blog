<?php

namespace AppBundle\Repository;

use AppBundle\Entity\Category;
use Doctrine\ORM\EntityRepository;

class PostRepository extends EntityRepository
{
    /**
     * @param $category_id
     *
     * @return array
     */
    public function findPosts(Category $category = null)
    {
        $qb = $this
            ->createQueryBuilder("p")
            ->select("c", "p")
            ->join("p.category", "c");

        if ($category !== null) {
            $qb->where("c.id = :category_id")
                ->setParameter("category_id", $category->getId());
        }

        return $qb->getQuery()->getResult();
    }
}