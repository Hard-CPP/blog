<?php

namespace AppBundle\Entity\Traits;

use Doctrine\ORM\Mapping as ORM;

trait Seo
{
    /**
     * @ORM\Column(type="string", length=80)
     */
    private $seoTitle;

    /**
     * @ORM\Column(type="string", length=140)
     */
    private $seoDescription;
}