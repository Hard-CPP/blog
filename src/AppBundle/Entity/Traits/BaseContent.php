<?php

namespace AppBundle\Entity\Traits;

trait BaseContent
{
    /**
     * @ORM\Column(type="string", length=255)
     */
    private $title;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $subTitle;

    /**
     * @ORM\Column(type="text")
     */
    private $content;
}