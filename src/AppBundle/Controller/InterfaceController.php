<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Page;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class InterfaceController extends Controller
{
    /**
     * @Route("/parts/menu/page", name="parts.menu.page")
     */
    public function pageMenuAction()
    {
        $pages = $this
            ->getDoctrine()
            ->getRepository(Page::class)
            ->findAll();

        return $this->render(
            "@App/interface/page_menu.html.twig",
            [
                "pages" => $pages
            ]
        );

    }
}