<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Category;
use AppBundle\Entity\Post;
use AppBundle\Form\CategoryType;
use AppBundle\Form\PostType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/admin")
 */
class AdminController extends Controller
{
    /**
     * @Route("/", name="admin.index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $categories = $this
            ->getDoctrine()
            ->getRepository(Category::class)
            ->findAll();

        $posts = $this
            ->getDoctrine()
            ->getRepository(Post::class)
            ->findAll();

        return $this->render(
            "@App/admin/index.html.twig",
            [
                "categories" => $categories,
                "posts"      => $posts
            ]
        );
    }

    /**
     * @Route("/category/{id}", name="admin.category", defaults={"id" = null})
     * @Method({"GET", "POST"})
     */
    public function categoryAction(Category $category = null, Request $request)
    {
        if ($category === null) {
            $category = new Category();
        }

        $form = $this->createForm(CategoryType::class, $category);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($category);
            $em->flush();

            return $this->redirectToRoute("admin.index");
        }

        return $this->render(
            "@App/admin/category.html.twig",
            [
                "category" => $category,
                "form"     => $form->createView()
            ]
        );
    }

    /**
     * @Route("/post/{id}", name="admin.post", defaults={"id" = null})
     */
    public function postAction(Post $post = null, Request $request)
    {
        if ($post === null) {
            $post = new Post();
        }

        $form = $this->createForm(PostType::class, $post);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($post);
            $em->flush();

            return $this->redirectToRoute("admin.index");
        }

        return $this->render(
            "@App/admin/post.html.twig",
            [
                "post" => $post,
                "form" => $form->createView()
            ]
        );
    }

    /**
     * @Route("/post/delete/{id}", name="admin.post.delete")
     *
     * @param Post $post
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function deletePostAction(Post $post)
    {
        $em = $this->getDoctrine()->getManager();
        $em->remove($post);
        $em->flush();

        return $this->redirectToRoute("admin.index");
    }

    /**
     * @Route("/category/delete/{id}", name="admin.category.delete")
     *
     * @param Category $category
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function categoryPostAction(Category $category)
    {
        $em = $this->getDoctrine()->getManager();
        $em->remove($category);
        $em->flush();

        return $this->redirectToRoute("admin.index");
    }

}