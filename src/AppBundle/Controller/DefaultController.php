<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Category;
use AppBundle\Entity\Page;
use AppBundle\Entity\Post;
use Doctrine\DBAL\Driver\AbstractPostgreSQLDriver;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        $posts = $this
            ->getDoctrine()
            ->getRepository(Post::class)
            ->findPosts();

        return $this->render(
            "@App/default/homepage.html.twig",
            [
                "header_image"    => "img/home-bg.jpg",
                "header_title"    => "Clean Blog",
                "header_baseline" => "A Blog Theme by Start Bootstrap",
                "posts"           => $posts
            ]
        );
    }

    /**
     * @Route("/post/{id}", name="page.post")
     */
    public function postAction($id)
    {
        $post = $this
            ->getDoctrine()
            ->getRepository(Post::class)
            ->find($id);

        if ($post === null) {
            //return $this->redirectToRoute("homepage");

            throw $this->createNotFoundException();
        }

        return $this->render(
            "@App/default/post.html.twig",
            [
                "post" => $post
            ]
        );
    }

    /**
     * @Route("/post/category/{id}", name="page.post.category")
     */
    public function postFilterAction($id)
    {
        $category = $this
            ->getDoctrine()
            ->getRepository(Category::class)
            ->findCategory($id);

        if ($category === null) {
            throw $this->createNotFoundException("Category not found");
        }

        return $this->render(
            "@App/default/post_filter.html.twig",
            [
                "category" => $category,
                "header_image"    => "img/home-bg.jpg",
                "header_title"    => "Clean Blog",
                "header_baseline" => "A Blog Theme by Start Bootstrap",
            ]
        );
    }

    /**
     * @Route("/page/{name}", name="page")
     */
    public function pageAction($name)
    {
        $page = $this
            ->getDoctrine()
            ->getRepository(Page::class)
            ->findOneBy(["name" => $name]);

        return $this->render(
            "@App/default/page.html.twig",
            ["page" => $page]
        );
    }

    /**
     * @Route("/test")
     */
    public function testAction()
    {
        dump($this->get("service.base.mailer"));
        dump($this->get("service.smart.mailer"));

        die();
    }
}
