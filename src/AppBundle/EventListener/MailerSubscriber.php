<?php

namespace AppBundle\EventListener;

use AppBundle\AppEvents;
use AppBundle\Event\ContactEvent;
use AppBundle\Services\Mailer\MailerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class MailerSubscriber implements EventSubscriberInterface
{
    private $mailer;
    private $sender;

    public function __construct(MailerInterface $mailer, string $sender)
    {
        $this->mailer = $mailer;
        $this->sender = $sender;
    }

    public static function getSubscribedEvents()
    {
        return [AppEvents::ON_CONTACT => "send"];
    }

    public function send(ContactEvent $event)
    {
        $contact = $event->getContact();

        $this
            ->mailer
            ->setSender($this->sender)
            ->setRecipient($contact->getEmail())
            ->setMessage($contact->getMessage())
            ->setObject("Demande de contact")
            ->send();
    }
}