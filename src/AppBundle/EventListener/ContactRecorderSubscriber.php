<?php

namespace AppBundle\EventListener;

use AppBundle\AppEvents;
use AppBundle\Event\ContactEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Doctrine\Bundle\DoctrineBundle\Registry as Doctrine;

class ContactRecorderSubscriber implements EventSubscriberInterface
{
    private $doctrine;

    public function __construct(Doctrine $doctrine)
    {
        $this->doctrine = $doctrine;
    }

    public static function getSubscribedEvents()
    {
        return [AppEvents::ON_CONTACT => "save"];
    }

    public function save(ContactEvent $event)
    {
        $em = $this->doctrine->getManager();
        $em->persist($event->getContact());
        $em->flush();
    }
}