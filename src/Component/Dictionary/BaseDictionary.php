<?php

namespace Component\Dictionary;

class BaseDictionary extends AbstractDictionary
{
    public function getDefinitions(): array
    {
        return [
            "center"  => "centre",
            "Moon"    => "Lune"
        ];
    }
}