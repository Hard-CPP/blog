<?php

namespace Component\Dictionary;

class AllDictionary extends AbstractDictionary
{
    /**
     * @var AbstractDictionary []
     */
    private $dictionaries;

    public function __construct(array $dictionaries)
    {
        $this->dictionaries = $dictionaries;
    }

    public function getDefinitions(): array
    {
        $output = [];
        foreach ($this->dictionaries as $dictionary) {
            if ($dictionary instanceof AbstractDictionary) {
                $output += $dictionary->getDefinitions();
            }
        }

        return $output;
    }
}