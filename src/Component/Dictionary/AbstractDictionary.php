<?php

namespace Component\Dictionary;

abstract class AbstractDictionary
{
    private $definitions = null;

    abstract public function getDefinitions(): array;

    public function getDefinition(string $word)
    {
        if ($this->definitions === null) {
            $this->definitions = $this->getDefinitions();
        }

        if (array_key_exists($word, $this->definitions)) {
            return $this->definitions[$word];
        }
    }
}