<?php

namespace Component\Dictionary;

use Doctrine\Bundle\DoctrineBundle\Registry as Doctrine;

class DbDictionary extends AbstractDictionary
{
    private $doctrine;

    public function __construct(Doctrine $doctrine)
    {
        $this->doctrine = $doctrine;
    }

    public function getDefinitions(): array
    {
        $results = $this->doctrine->getRepository("AppBundle:Dictionary")->findAll();

        $output = [];
        foreach($results as $result) {
            $output[$result->getLabel()] = $result->getDefinition();
        }

        return $output;
    }



}