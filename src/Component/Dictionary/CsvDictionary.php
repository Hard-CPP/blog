<?php

namespace Component\Dictionary;

class CsvDictionary extends AbstractDictionary
{
    public function getDefinitions(): array
    {
        $dataSource = __DIR__ . DIRECTORY_SEPARATOR . "data/dictionary.csv";

        $output = [];

        if (file_exists($dataSource)) {
            $data = file($dataSource);

            foreach ($data as $entry) {
                $tmp = explode(";", $entry);
                if (count($tmp) == 2) {
                    $output[$tmp[0]] = str_replace(["\n", "\r"], "", $tmp[1]);
                }
            }

            return $output;

        }

        return [];
    }

}